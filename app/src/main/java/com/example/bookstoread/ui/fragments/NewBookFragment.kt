package com.example.bookstoread.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.activityViewModels
import com.example.bookstoread.R
import com.example.bookstoread.databinding.FragmentBookRecyclerBinding
import com.example.bookstoread.databinding.FragmentNewBookBinding
import com.example.bookstoread.db.repository.Book
import com.example.bookstoread.ui.viewmodels.BookViewModel

class NewBookFragment : Fragment() {

    private val bookViewModel: BookViewModel by activityViewModels()
    private lateinit var binding: FragmentNewBookBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = FragmentNewBookBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val categoryItems = listOf("Action", "Terror", "Romance")
        val categoryAdapter = ArrayAdapter(requireContext(), R.layout.category_item, categoryItems)
        binding.bookCategoryTextView.setAdapter(categoryAdapter)

        val statusItems = listOf("Read", "To Read", "Reading")
        val statusAdapter = ArrayAdapter(requireContext(), R.layout.category_item, statusItems)
        binding.bookStatusTextView.setAdapter(statusAdapter)

        binding.newBookButton.setOnClickListener {
            bookViewModel.addBook(
                Book(
                    0,
                    binding.bookTitleField.text.toString(),
                    binding.bookDescriptionField.text.toString(),
                    binding.bookCategoryTextView.text.toString(),
                    binding.bookStatusTextView.text.toString()
                )
            )
            binding.bookTitleField.setText("")
            binding.bookDescriptionField.setText("")
            binding.bookCategoryTextView.setText("")
            binding.bookStatusTextView.setText("")
        }

    }
}