package com.example.bookstoread.ui.adapters

import android.content.Context
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.bookstoread.R
import com.example.bookstoread.databinding.ItemBookBinding
import com.example.bookstoread.db.repository.Book
import com.example.bookstoread.ui.OnClickListener

class BookAdapter(private var bookList: List<Book>, private val listener: OnClickListener):
    RecyclerView.Adapter<BookAdapter.ViewHolder>() {

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_book, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val book = bookList[position]
        with(holder) {
            setListener(book)
            binding.titleTextView.text = book.title
            binding.descriptionTextView.text = book.description
            binding.categoryTextView.text = book.category
            binding.statusTextView.text = book.status
        }
    }

    override fun getItemCount(): Int {
        return bookList.size
    }

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val binding = ItemBookBinding.bind(view)
        fun setListener(book: Book) {
            binding.root.setOnClickListener {
                listener.onClick(book)
            }
        }
    }

    fun setList(newBookList: List<Book>) {
        bookList = newBookList
        notifyDataSetChanged()
    }
}