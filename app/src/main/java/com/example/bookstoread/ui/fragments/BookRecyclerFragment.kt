package com.example.bookstoread.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.bookstoread.R
import com.example.bookstoread.ui.viewmodels.BookViewModel
import com.example.bookstoread.ui.OnClickListener
import com.example.bookstoread.databinding.FragmentBookRecyclerBinding
import com.example.bookstoread.db.repository.Book
import com.example.bookstoread.ui.adapters.BookAdapter


class BookRecyclerFragment : Fragment(), OnClickListener {

    private lateinit var bookAdapter: BookAdapter
    private lateinit var gridLayoutManager: RecyclerView.LayoutManager
    private val bookViewModel: BookViewModel by activityViewModels()
    private lateinit var binding: FragmentBookRecyclerBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = FragmentBookRecyclerBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bookViewModel.getAllBooks()

        setUpRecyclerView(bookViewModel.bookList.value!!.toList())
        bookViewModel.bookList.observe(viewLifecycleOwner, Observer {
            bookAdapter.setList(it)

            if (bookViewModel.bookList.value == null) {
                Toast.makeText(context, "ERROR", Toast.LENGTH_SHORT).show()
            } else {
                setUpRecyclerView(it)
            }
        })
    }

    private fun setUpRecyclerView(bookList: List<Book>) {
        gridLayoutManager = GridLayoutManager(context, 1)
        if (bookList != null) {
            bookAdapter = BookAdapter(bookList, this)
            binding.recyclerView.apply {
                setHasFixedSize(true)
                layoutManager = gridLayoutManager
                adapter = bookAdapter
            }
        }
    }


    override fun onClick(book: Book) {
        bookViewModel.select(book)
        findNavController().navigate(R.id.action_BookRecyclerFragment_to_DetailFragment)
    }
}