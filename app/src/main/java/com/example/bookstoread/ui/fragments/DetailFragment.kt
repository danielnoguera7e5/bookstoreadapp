package com.example.bookstoread.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.example.bookstoread.R
import com.example.bookstoread.databinding.FragmentDetailBinding
import com.example.bookstoread.databinding.FragmentNewBookBinding
import com.example.bookstoread.ui.viewmodels.BookViewModel


class DetailFragment : Fragment() {

    private val bookViewModel: BookViewModel by activityViewModels()
    private lateinit var binding: FragmentDetailBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = FragmentDetailBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bookViewModel.selectedBook.observe(viewLifecycleOwner, Observer {
            binding.bookTitleView.text = it.title
            binding.bookDescriptionView.text = it.description
            binding.bookCategoryTextView.text = it.category
            binding.bookStatusTextView.text = it.status
        })

        binding.deleteButton.setOnClickListener {
            bookViewModel.delete(bookViewModel.selectedBook.value!!)
            Navigation.findNavController(binding.root).navigate(R.id.BookRecyclerFragment)
        }

    }
}