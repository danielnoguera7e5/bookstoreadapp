package com.example.bookstoread.ui

import com.example.bookstoread.db.repository.Book

interface OnClickListener {
    fun onClick(book: Book)
}