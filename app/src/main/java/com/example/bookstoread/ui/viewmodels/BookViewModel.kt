package com.example.bookstoread.ui.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.bookstoread.db.repository.Book
import com.example.bookstoread.db.repository.ServiceLocator
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class BookViewModel: ViewModel() {

    val bookList = MutableLiveData<MutableList<Book>>(mutableListOf())
    val selectedBook = MutableLiveData<Book>()

    init {
        getAllBooks()
    }

    fun getAllBooks() {
        ServiceLocator.bookRepository.getAllBooks()
        android.os.Handler().postDelayed({
            bookList.postValue(ServiceLocator.bookRepository.BookList.value!!.toMutableList())
        }, 200)
    }

    fun select(book: Book) {
        selectedBook.postValue(book)
    }

    fun delete(book: Book) {
        ServiceLocator.bookRepository.deleteBook(book.id)
    }

    fun deleteAll() {
        ServiceLocator.bookRepository
    }

    fun addBook(book: Book) {
        ServiceLocator.bookRepository.addBook(book)
    }
}