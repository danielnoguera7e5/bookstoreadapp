package com.example.bookstoread.db.room

import androidx.lifecycle.MutableLiveData
import com.example.bookstoread.db.repository.Book
import com.example.bookstoread.db.repository.BookRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class BookRepositoryRoom(override val BookList: MutableLiveData<List<Book>>) : BookRepository {

    val bookDao = BookApplication.database.bookDao()

    override fun getAllBooks(): List<Book> {
        val bookList = mutableListOf<Book>()
        CoroutineScope(Dispatchers.IO).launch {
             bookDao.getAllBooks().forEach {
                 bookList.add(bookEntitytoBook(it))
             }
        }
        BookList.value = bookList
        return BookList.value!!
    }


    override fun addBook(book: Book) {
        CoroutineScope(Dispatchers.IO).launch {
            bookDao.addBook(booktoBookEntity(book))
        }
    }

    override fun getBook(id: Int): Book? {
        var book: Book? = null
        CoroutineScope(Dispatchers.IO).launch {
            book = bookEntitytoBook(bookDao.getBookById(id.toLong()))
        }
        return book
    }

    override fun deleteBook(id: Int) {
        CoroutineScope(Dispatchers.IO).launch {
              bookDao.deleteBook(bookDao.getBookById(id.toLong()))
        }
    }

    override fun deleteAllBooks() {
        CoroutineScope(Dispatchers.IO).launch {
            bookDao.deleteAllBooks()
        }
    }

    fun booktoBookEntity(book: Book):BookEntity {
        return BookEntity(
            book.id.toLong(),
            book.title,
            book.description,
            book.category,
            book.status
        )
    }

    fun bookEntitytoBook(bookEntity: BookEntity): Book {
        return Book(
            bookEntity.id.toInt(),
            bookEntity.title,
            bookEntity.description,
            bookEntity.category,
            bookEntity.status
        )
    }

}