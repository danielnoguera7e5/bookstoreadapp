package com.example.bookstoread.db.room

import android.app.Application
import androidx.room.Room

class BookApplication: Application() {
    companion object {
        lateinit var database: BookDatabase
    }

    override fun onCreate() {
        super.onCreate()
        database = Room.databaseBuilder(
            this,
            BookDatabase::class.java,
            "CharacterDatabase").build()
    }
}