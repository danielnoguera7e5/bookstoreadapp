package com.example.bookstoread.db.room

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query


@Dao
interface BookDao {

    @Query("SELECT * FROM BookEntity")
    fun getAllBooks(): List<BookEntity>

    @Insert()
    fun addBook(bookEntity: BookEntity)

    @Delete()
    fun deleteBook(bookEntity: BookEntity)

    @Query("SELECT * FROM BookEntity where id = :id ORDER BY id DESC LIMIT 1")
    fun getBookById(id: Long): BookEntity

    @Query("DELETE FROM BookEntity")
    fun deleteAllBooks()
}