package com.example.bookstoread.db.repository

data class Book(
    val id: Int,
    val title: String,
    val description: String,
    val status: String,
    val category: String
)