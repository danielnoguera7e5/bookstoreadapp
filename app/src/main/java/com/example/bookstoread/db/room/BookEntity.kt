package com.example.bookstoread.db.room

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "BookEntity")
data class BookEntity(
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0,
    var title: String,
    var description: String,
    var status: String,
    var category: String)