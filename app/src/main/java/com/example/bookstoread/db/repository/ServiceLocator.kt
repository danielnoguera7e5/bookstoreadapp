package com.example.bookstoread.db.repository

import androidx.lifecycle.MutableLiveData
import com.example.bookstoread.db.room.BookRepositoryRoom

object ServiceLocator {

    val bookRepository: BookRepository = BookRepositoryRoom(MutableLiveData<List<Book>>())

}