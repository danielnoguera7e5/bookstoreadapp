package com.example.bookstoread.db.repository

import androidx.lifecycle.MutableLiveData


interface BookRepository {

    val BookList: MutableLiveData<List<Book>>

    fun addBook(book: Book)

    fun getBook(id: Int): Book?

    fun deleteBook(id: Int)

    fun getAllBooks(): List<Book>

    fun deleteAllBooks()
}